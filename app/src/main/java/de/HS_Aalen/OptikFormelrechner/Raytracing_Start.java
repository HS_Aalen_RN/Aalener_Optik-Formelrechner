package de.HS_Aalen.OptikFormelrechner;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.content.Context;


public class Raytracing_Start extends AppCompatActivity {

    final Context context = this;
    private EditText et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.raytracing_start);

        // Verhindert das Öffnen des Tastaturlayouts beim Starten der Activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        et = (EditText) findViewById(R.id.eT);
        Button bt = (Button) findViewById(R.id.bT);

        bt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String enteredText = et.getText().toString();

                // Sollte keine Zahl eingegeben werden, so wird das Layout für eine einzelne
                // Fläche erstellt
                if (enteredText.equals("") || enteredText.equals("0")) {
                    enteredText = "1";
                }

                // Beschränkt die Berechnung auf maximal 1000 Flächen und gibt bei einer größeren
                // Zahl einen Warnhinweis
                int Overload = Integer.parseInt(enteredText);
                if (Overload > 1000) {
                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                    AlarmBox.setTitle("Obere Grenze");
                    AlarmBox.setMessage("Es können maximal 1000 Flächen berechnet werden");
                    AlarmBox.setNeutralButton("OK", null);

                    AlertDialog dialog = AlarmBox.create();
                    dialog.show();
                    return;
                }

                // Die Bundle Funktion ermöglicht die Weitergabe der Anzahl der zu berechnenden
                // Flächen an die nächste Activity
                // Zudem wird der Status der Checkbox ebenfalls weitergeleitet
                Bundle korb = new Bundle();
                korb.putString("datenpaket1", enteredText);
                Intent in = new Intent(Raytracing_Start.this, Raytracing_Berechnung.class);
                in.putExtras(korb);

                final CheckBox checkBox = (CheckBox) findViewById(R.id.check_s1);
                Bundle korb2 = new Bundle();
                if (checkBox.isChecked()) {
                    korb2.putString("datenpaket2", "1");
                } else {
                    korb2.putString("datenpaket2", "0");
                }
                in.putExtras(korb2);
                startActivity(in);

                }
        });
    }
}