package de.HS_Aalen.OptikFormelrechner;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import static android.util.TypedValue.COMPLEX_UNIT_DIP;


public class Raytracing_Berechnung extends AppCompatActivity {

    final Context context = this;
    EditText B1,B2,B3,B4,B5,Z1,Z2,Z3,Z4,VN1,VNM1;
    TextView Ausgabe, Ausgabe_zwischenergebnisse;
    double S,S_1,S_2,N1,N2,N5,R1,D1,S1,S2,A,B,C,D2,f_1,D,f,E,Z_R1,Z_N1,Z_N2,Z_D,Z_S_1,Z_B,Z_A,
            sH,sH_1,knoten,knoten_1,Vgr,Vgr_T,Vgr_W,Vgr_N1,Vgr_Nm1,D_Ges;
    int zwischenspeicher;
    String formation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.raytracing_berechnung);

        // Empfängt die gesendeten Inhalte aus der Bundle Funktion
        Bundle zielkorb = getIntent().getExtras();
        String text2 = zielkorb.getString("datenpaket1");
        final int fn = Integer.parseInt(text2);
        Bundle zielkorb2 = getIntent().getExtras();
        String text3 = zielkorb2.getString("datenpaket2");
        final int cb_s1 = Integer.parseInt(text3);

        // Verhindert das Öffnen des Tastaturlayouts beim Starten der Activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // Erstellt im folgenden ein komplett dynamisches Layout auf Basis der Anzahl der Flächen
        ScrollView sv = new ScrollView(this);
        LinearLayout ll = new LinearLayout(this);

        ll.setOrientation(LinearLayout.VERTICAL);

        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.setMargins(200,0,0,0);

        sv.addView(ll);

        int k = 1;

        // Erstellt an dieser Stelle die TextView und EditText Elemente die zur Eingabe benötigt
        // werden und weißt jedem Element eine eigene ID zu
        for (int i = 0; i < fn; i++) {
            TextView tV0 = new TextView(this);
            tV0.setPadding(20,0,0,0);
            tV0.setLines(3);
            tV0.setGravity(Gravity.CENTER_VERTICAL);
            tV0.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
            TextView tVa = new TextView(this);
            tVa.setPadding(100,0,0,5);
            TextView tVb = new TextView(this);
            tVb.setPadding(100,0,0,5);
            TextView tVc = new TextView(this);
            tVc.setPadding(100,0,0,5);
            TextView tVd = new TextView(this);
            tVd.setPadding(100,0,0,5);
            TextView tVe = new TextView(this);
            tVe.setPadding(100,0,0,5);
            TextView tVf = new TextView(this);
            tVf.setLines(3);
            tVf.setPadding(20,0,0,0);
            tVf.setGravity(Gravity.CENTER_VERTICAL);
            tVf.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);


            EditText et1 = new EditText(this);
            et1.setLayoutParams(params);
            et1.setPadding(20,0,20,20);
            et1.setEms(4);
            et1.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            et1.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED |
                    InputType.TYPE_NUMBER_FLAG_DECIMAL);
            et1.setId(k);
            k = k + 1;
            EditText et2 = new EditText(this);
            et2.setLayoutParams(params);
            et2.setPadding(20,0,20,20);
            et2.setEms(4);
            et2.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            et2.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            et2.setId(k);
            k = k + 1;
            EditText et3 = new EditText(this);
            et3.setLayoutParams(params);
            et3.setPadding(20,0,20,20);
            et3.setEms(4);
            et3.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            et3.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            et3.setId(k);
            k = k + 1;
            EditText et4 = new EditText(this);
            et4.setLayoutParams(params);
            et4.setPadding(20,0,20,20);
            et4.setEms(4);
            et4.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            et4.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED |
                    InputType.TYPE_NUMBER_FLAG_DECIMAL);
            et4.setId(k);
            k = k + 1;
            EditText et5 = new EditText(this);
            et5.setLayoutParams(params);
            et5.setPadding(20,0,20,20);
            et5.setEms(4);
            et5.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            et5.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            et5.setId(k);
            k = k + 1;

            tV0.setText("Fläche: " + (i + 1));
            // Benötigt vor der ersten Fläche die Felder für den Objektabstand s und
            // den Brechungsindex n
            if (i == 0) {
                tVa.setText(Html.fromHtml("s" + "<sub><small>" + (i + 1) + "</small></sub> [mm]"));
                tVb.setText(Html.fromHtml("n" + "<sub><small>" + (i + 1) + "</small></sub>"));
            }
            tVc.setText(Html.fromHtml("n" + "<sub><small>" + (i + 2) + "</small></sub>"));
            tVd.setText(Html.fromHtml("r" + "<sub><small>" + (i + 1) + "</small></sub> [mm]"));
            tVe.setText(Html.fromHtml("d" + "<sub><small>" + (i + 1) + "</small></sub> [mm]"));
            tVf.setText("Abstand zwischen Fläche " + (i+1) + " & " + (i+2));


            ll.addView(tV0);
            if (i == 0) {

                //Fügt die Eingabe für den Objektabstand s nur ein, wenn der Status der
                // Checkbox in der vorigen Activity "false" war
                if (cb_s1 == 0) {
                    ll.addView(tVa);
                    ll.addView(et1);
                }
                ll.addView(tVb);
                ll.addView(et2);
            }
            ll.addView(tVc);
            ll.addView(et3);
            ll.addView(tVd);
            ll.addView(et4);
            //Lässt das Eingabefeld und die Beschreibung für den Abstand zwischen den Flächen
            // "d" nach der letzten Fläche weg
            if (i < fn - 1) {
                ll.addView(tVf);
                ll.addView(tVe);
                ll.addView(et5);
            }
        }

        Button bt = new Button(this);
        bt.setText("Berechnen");
        bt.setAllCaps(false);
        ll.addView(bt);

        //Erstellt das das finale TextView in der das Ergebnis anschließend dargestellt wird
        final TextView tv = new TextView(this);
        tv.setText("");
        tv.setId(0);
        tv.setVisibility(View.GONE);
        tv.setWidth(200);
        tv.setTextSize(COMPLEX_UNIT_DIP, 20);
        tv.setPadding(100,50,0,50);
        ll.addView(tv);

            //Erstellt das TextView in der die Zwischenergebnisse fargestellt werden
            final TextView zwischenergebnisse = new TextView(this);
            zwischenspeicher = fn*5+1;
            zwischenergebnisse.setId(zwischenspeicher);
            zwischenergebnisse.setMaxLines(fn*2-1);
            zwischenergebnisse.setPadding(15,0,0,15);
            zwischenergebnisse.setVisibility(View.GONE);
            zwischenergebnisse.setTextSize(COMPLEX_UNIT_DIP, 17);
            ll.addView(zwischenergebnisse);


        this.setContentView(sv);


        bt.setOnClickListener (new View.OnClickListener() {
            public void onClick(View v) {


                int j = 1;
                int absolut = 1;
                tv.setVisibility(View.VISIBLE);
                f_1 = 1;
                if (fn > 1) {
                    zwischenergebnisse.setVisibility(View.VISIBLE);
                    if (fn == 2) {
                        formation = "Zwischenergebnis: <br>";
                    } else {
                        formation = "Zwischenergebnisse: <br>";
                    }
                }

                // Berechnung wird für die Anzahl "fn" der Flächen durchgeführt
                for ( int k = 0; k < fn; k++) {

                    if (k == 0) {

                        // 1. Fall => Objektabstand s ist -unendlich
                        if (cb_s1 == 0) {

                            //Abfrage ob das Eingabegeld leer ist mit Warnhinweis
                            B1 = (EditText) findViewById(j);
                            String Alarm = B1.getText().toString();
                            if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                AlarmBox.setTitle("Leeres Feld!");
                                AlarmBox.setMessage(Html.fromHtml("s<sub><small>1</small>" +
                                        "</sub> ist leer<br>"));
                                AlarmBox.setNeutralButton("OK", null);

                                AlertDialog dialog = AlarmBox.create();
                                dialog.show();
                                tv.setVisibility(View.GONE);
                                zwischenergebnisse.setVisibility(View.GONE);
                                return;
                            }
                            double S1 = Double.parseDouble(B1.getText().toString());
                            j = j + 1;
                            B2 = (EditText) findViewById(j);
                            Alarm = B2.getText().toString();
                            if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                AlarmBox.setTitle("Leeres Feld!");
                                AlarmBox.setMessage(Html.fromHtml("n<sub><small>1</small>" +
                                        "</sub> ist leer<br>"));
                                AlarmBox.setNeutralButton("OK", null);

                                AlertDialog dialog = AlarmBox.create();
                                dialog.show();
                                tv.setVisibility(View.GONE);
                                zwischenergebnisse.setVisibility(View.GONE);
                                return;
                            }
                            double N1 = Double.parseDouble(B2.getText().toString());
                            j = j + 1;
                            B3 = (EditText) findViewById(j);
                            Alarm = B3.getText().toString();
                            if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                AlarmBox.setTitle("Leeres Feld!");
                                AlarmBox.setMessage(Html.fromHtml("n<sub><small>2</small>" +
                                        "</sub> ist leer<br>"));
                                AlarmBox.setNeutralButton("OK", null);

                                AlertDialog dialog = AlarmBox.create();
                                dialog.show();
                                tv.setVisibility(View.GONE);
                                zwischenergebnisse.setVisibility(View.GONE);
                                return;
                            }
                            double N2 = Double.parseDouble(B3.getText().toString());
                            j = j + 1;
                            B4 = (EditText) findViewById(j);
                            Alarm = B4.getText().toString();
                            if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                AlarmBox.setTitle("Leeres Feld!");
                                AlarmBox.setMessage(Html.fromHtml("r<sub><small>1</small>" +
                                        "</sub> ist leer<br>"));
                                AlarmBox.setNeutralButton("OK", null);

                                AlertDialog dialog = AlarmBox.create();
                                dialog.show();
                                tv.setVisibility(View.GONE);
                                zwischenergebnisse.setVisibility(View.GONE);
                                return;
                            }
                            double R1 = Double.parseDouble(B4.getText().toString());
                            j = j + 1;
                            // So lange noch nicht die letzte Fläche erreicht wurde, wird der
                            // Abstand zwischen den Flächen "d" eingelesen
                            if (k < fn - 1) {
                                B5 = (EditText) findViewById(j);
                                Alarm = B5.getText().toString();
                                if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                        Alarm.equals("+")) {
                                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                    AlarmBox.setTitle("Leeres Feld!");
                                    AlarmBox.setMessage(Html.fromHtml("d<sub><small>1</small>" +
                                            "</sub> ist leer<br>"));
                                    AlarmBox.setNeutralButton("OK", null);

                                    AlertDialog dialog = AlarmBox.create();
                                    dialog.show();
                                    tv.setVisibility(View.GONE);
                                    zwischenergebnisse.setVisibility(View.GONE);
                                    return;
                                }
                                double D1 = Double.parseDouble(B5.getText().toString());
                                j = j + 1;
                            }

                            S_1 = (N1 / S1 + (N2 - N1) / R1) / N2;
                            B = N2;

                            Vgr = (1/S_1)/S1;

                            //2. Fall => Objektabstand wurde manuell eingegeben
                        } else if (cb_s1 == 1) {
                            j = j + 1;
                            B2 = (EditText) findViewById(j);
                            String Alarm = B2.getText().toString();
                            if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                AlarmBox.setTitle("Leeres Feld!");
                                AlarmBox.setMessage(Html.fromHtml("n<sub><small>1</small>" +
                                        "</sub> ist leer<br>"));
                                AlarmBox.setNeutralButton("OK", null);

                                AlertDialog dialog = AlarmBox.create();
                                dialog.show();
                                tv.setVisibility(View.GONE);
                                zwischenergebnisse.setVisibility(View.GONE);
                                return;
                            }
                            double N1 = Double.parseDouble(B2.getText().toString());
                            j = j + 1;
                            B3 = (EditText) findViewById(j);
                            Alarm = B3.getText().toString();
                            if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                AlarmBox.setTitle("Leeres Feld!");
                                AlarmBox.setMessage(Html.fromHtml("n<sub><small>2</small>" +
                                        "</sub> ist leer<br>"));
                                AlarmBox.setNeutralButton("OK", null);

                                AlertDialog dialog = AlarmBox.create();
                                dialog.show();
                                tv.setVisibility(View.GONE);
                                zwischenergebnisse.setVisibility(View.GONE);
                                return;
                            }
                            double N2 = Double.parseDouble(B3.getText().toString());
                            j = j + 1;
                            B4 = (EditText) findViewById(j);
                            Alarm = B4.getText().toString();
                            if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                AlarmBox.setTitle("Leeres Feld!");
                                AlarmBox.setMessage(Html.fromHtml("r<sub><small>1</small>" +
                                        "</sub> ist leer<br>"));
                                AlarmBox.setNeutralButton("OK", null);

                                AlertDialog dialog = AlarmBox.create();
                                dialog.show();
                                tv.setVisibility(View.GONE);
                                zwischenergebnisse.setVisibility(View.GONE);
                                return;
                            }
                            double R1 = Double.parseDouble(B4.getText().toString());
                            j = j + 1;
                            if (k < fn - 1) {
                                B5 = (EditText) findViewById(j);
                                Alarm = B5.getText().toString();
                                if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                        Alarm.equals("+")) {
                                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                    AlarmBox.setTitle("Leeres Feld!");
                                    AlarmBox.setMessage(Html.fromHtml("d<sub><small>1</small>" +
                                            "</sub> ist leer<br>"));
                                    AlarmBox.setNeutralButton("OK", null);

                                    AlertDialog dialog = AlarmBox.create();
                                    dialog.show();
                                    tv.setVisibility(View.GONE);
                                    zwischenergebnisse.setVisibility(View.GONE);
                                    return;
                                }
                                double D1 = Double.parseDouble(B5.getText().toString());
                                j = j + 1;
                            }

                            S_1 = ((N2 - N1) / R1) / N2;
                            B = N2;
                            D = S_1 - D1;
                            f_1 = (S_1/D);


                        }
                        // Nachdem die erste Fläche berechnet wurde, ist die Berechnung für die
                        // restlichen Flächen sowohl für den 1. als auch für den 2. Fall identisch
                    } else {
                        j = j + 2;
                        B3 = (EditText) findViewById(j);
                        String Alarm = B3.getText().toString();
                        if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                Alarm.equals("+")) {
                            AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                            AlarmBox.setTitle("Leeres Feld!");
                            AlarmBox.setMessage(Html.fromHtml("n<sub><small>" + (k+2) +
                                    "</small></sub> ist leer<br>"));
                            AlarmBox.setNeutralButton("OK", null);

                            AlertDialog dialog = AlarmBox.create();
                            dialog.show();
                            tv.setVisibility(View.GONE);
                            zwischenergebnisse.setVisibility(View.GONE);
                            return;
                        }
                        double N2 = Double.parseDouble(B3.getText().toString());
                        j = j + 1;
                        B4 = (EditText) findViewById(j);
                        Alarm = B4.getText().toString();
                        if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                Alarm.equals("+")) {
                            AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                            AlarmBox.setTitle("Leeres Feld!");
                            AlarmBox.setMessage(Html.fromHtml("r<sub><small>" + (k+1) +
                                    "</small></sub> ist leer<br>"));
                            AlarmBox.setNeutralButton("OK", null);

                            AlertDialog dialog = AlarmBox.create();
                            dialog.show();
                            tv.setVisibility(View.GONE);
                            zwischenergebnisse.setVisibility(View.GONE);
                            return;
                        }
                        double R1 = Double.parseDouble(B4.getText().toString());
                        j = j + 1;
                        if (k < fn - 1 ) {
                            B5 = (EditText) findViewById(j);
                            Alarm = B5.getText().toString();
                            if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                AlarmBox.setTitle("Leeres Feld!");
                                AlarmBox.setMessage(Html.fromHtml("d<sub><small>" + (k+1) +
                                        "</small></sub> ist leer<br>"));
                                AlarmBox.setNeutralButton("OK", null);

                                AlertDialog dialog = AlarmBox.create();
                                dialog.show();
                                tv.setVisibility(View.GONE);
                                zwischenergebnisse.setVisibility(View.GONE);
                                return;
                            }
                            double D1 = Double.parseDouble(B5.getText().toString());
                            j = j + 1;

                        }
                        S_1 = (B / A + (N2 - B) / R1) / N2;
                        B = N2;
                        Vgr = Vgr * ((1/S_1)/A);

                    }
                    S_1 = 1 / S_1;


                    A = S_1;
                    // So lange nicht die letzte Fläche erreicht wurde, schreibe die Ergebnisse
                    // für s' und s in die Zwischenergebnisse
                    if (k < fn-1) {
                        formation = formation + "s'" + "<sub><small>" + (k+1) + "</small></sub>" +
                                " = " + Double.toString(Math.round(A*10000)/10000.0) + " mm<br>";
                        D2 = Double.parseDouble(B5.getText().toString());
                        C = S_1 - D2;
                        formation = formation + "s" + "<sub><small>" + (k + 2) + "</small></sub>" +
                                " = " + Double.toString(Math.round(C * 10000) / 10000.0) +
                                " mm<br>";
                        Ausgabe_zwischenergebnisse = (TextView) findViewById(zwischenspeicher);
                        Ausgabe_zwischenergebnisse.setText(Html.fromHtml(formation));
                    }

                    // So lange nicht die letzte Fläche erreicht wurde, berechne den neuen
                    // Objektabstand A "s" mit Hilfe des Abstandes zwischen die Flächen D1 "d"
                        if (k < fn - 1) {
                            double D1 = Double.parseDouble(B5.getText().toString());
                            A = S_1 - D1;
                            f_1 = f_1 * (S_1/A);
                        }

                    }

                f_1 = f_1 * A;
                sH_1 = A - f_1;
                // Wurde der Objektabstand manuell eingegeben, berechne ebenfalls die
                // Tiefenvergrößerung, latereale Vergrößerung und Winkelvergrößerung
                if (cb_s1 == 0) {
                    VN1 = (EditText) findViewById(2);
                    Vgr_N1 = Double.parseDouble(VN1.getText().toString());
                    VNM1 = (EditText) findViewById(((fn-1)*5)+3);
                    Vgr_Nm1 = Double.parseDouble(VNM1.getText().toString());
                    Vgr = Vgr * (Vgr_N1/Vgr_Nm1);
                    Vgr_T = (Vgr*Vgr*(Vgr_Nm1/Vgr_N1));
                    Vgr_W = ((1/Vgr)*(Vgr_N1/Vgr_Nm1));
                }


                // Für den Fall, dass der Objektabstand -unendlich war, berechne die Lage
                // der Hauptpunkte, die Knotenpunkte, sowie f und f'
                if (cb_s1 == 1 && fn > 1) {
                    absolut = fn*5-1;
                    //Um die objektseitige Brennweite f, k und die Lage des objektseitigen
                    // Hauptpunktes zu ermitteln, muss rückwärts durch das System gerechnet werden
                    for (int k = fn; k > 0; k--) {
                        if (k == fn) {
                            Z4 = (EditText) findViewById(absolut);
                            Z_R1 = Double.parseDouble(Z4.getText().toString());
                            Z_R1 = -Z_R1;
                            absolut = absolut - 1;

                            Z3 = (EditText) findViewById(absolut);
                            Z_N1 = Double.parseDouble(Z3.getText().toString());
                            absolut = absolut - 3;

                            Z1 = (EditText) findViewById(absolut);
                            Z_D = Double.parseDouble(Z1.getText().toString());
                            absolut = absolut - 2;

                            Z2 = (EditText) findViewById(absolut);
                            Z_N2 = Double.parseDouble(Z2.getText().toString());
                            absolut = absolut + 1;

                            Z_S_1 = ((Z_N2-Z_N1)/Z_R1)/Z_N2;
                            Z_S_1=1/Z_S_1;
                            Z_B = Z_N2;
                            Z_A = Z_S_1-Z_D;
                            f = Z_S_1/Z_A;

                        } else if (k > 1) {
                            Z4 = (EditText) findViewById(absolut);
                            Z_R1 = Double.parseDouble(Z4.getText().toString());
                            Z_R1 = -Z_R1;
                            absolut = absolut - 4;

                            Z1 = (EditText) findViewById(absolut);
                            Z_D = Double.parseDouble(Z1.getText().toString());
                            absolut = absolut - 2;

                            Z2 = (EditText) findViewById(absolut);
                            Z_N2 = Double.parseDouble(Z2.getText().toString());
                            absolut = absolut + 1;

                            Z_S_1 = ((Z_B/Z_A)+((Z_N2-Z_B)/Z_R1))/Z_N2;
                            Z_S_1 = 1 / Z_S_1;
                            Z_B = Z_N2;
                            Z_A = Z_S_1 - Z_D;
                            f = f * (Z_S_1/Z_A);

                        } else if (k == 1) {
                            Z4 = (EditText) findViewById(absolut);
                            Z_R1 = Double.parseDouble(Z4.getText().toString());
                            Z_R1 = -Z_R1;
                            absolut = absolut - 2;

                            Z2 = (EditText) findViewById(absolut);
                            Z_N2 = Double.parseDouble(Z2.getText().toString());

                            Z_S_1 = ((Z_B/Z_A)+((Z_N2-Z_B)/Z_R1))/Z_N2;
                            Z_S_1 = 1 / Z_S_1;
                            f = f * Z_S_1;
                            Z_S_1 = -Z_S_1;
                            f = -f;
                            sH = Z_S_1 - f;

                            knoten = f + f_1;
                            knoten_1 = f_1 + f;

                            VNM1 = (EditText) findViewById(((fn-1)*5)+3);
                            Vgr_Nm1 = Double.parseDouble(VNM1.getText().toString());
                            D_Ges = (Vgr_Nm1/(f_1/1000));
                        }
                    }
                }




                Ausgabe = (TextView) findViewById(0);


                if (cb_s1 == 1) {
                    if (fn == 1) {
                        Ausgabe.setText(Html.fromHtml("s'<sub><small>f'</small></sub> = " +
                                Double.toString(Math.round(A * 10000.0) / 10000.0) + " mm"));
                    } else {
                        Ausgabe.setText(Html.fromHtml("Flächen: " + fn +
                                "<br>s'<sub><small>f'</small></sub> = " +
                                Double.toString(Math.round(A * 10000.0) / 10000.0) +
                                " mm<br>" + "f' = " + Double.toString(Math.round(
                                        f_1 * 10000.0) / 10000.0) +" mm<br>" +
                                "D<sub><small>Ges</small></sub> = " +
                                Double.toString(Math.round(D_Ges * 10000.0) / 10000.0) +
                                " dpt<br>" + "s'<sub><small>H'</small></sub> = " +
                                Double.toString(Math.round(sH_1 * 10000.0) / 10000.0) +
                                " mm<br>" + "k' = " +
                                Double.toString(Math.round(knoten_1 * 10000.0) / 10000.0) +
                                " mm<br><br>" + "s<sub><small>f</small></sub> = " +
                                Double.toString(Math.round(Z_S_1 * 10000.0) / 10000.0) +
                                " mm<br>" + "f = " +
                                Double.toString(Math.round(f * 10000.0) / 10000.0) +
                                " mm<br>" + "s<sub><small>H</small></sub> = " +
                                Double.toString(Math.round(sH * 10000.0) / 10000.0) +
                                " mm<br>" + "k = " +
                                Double.toString(Math.round(knoten * 10000.0) / 10000.0) + " mm"));
                        Ausgabe_zwischenergebnisse = (TextView) findViewById(zwischenspeicher);

                    }
                } else {
                    if (fn == 1) {
                        Ausgabe.setText(Html.fromHtml("s'<sub><small>" + fn + "</small></sub> = " +
                                Double.toString(Math.round(A * 10000.0) / 10000.0) +
                                " mm<br><br>" + "ß = " +
                                Double.toString(Math.round(Vgr * 10000.0) / 10000.0) +
                                "<br>ß<sub><small>t</small></sub> = " +
                                Double.toString(Math.round(Vgr_T * 10000.0) / 10000.0) +
                                "<br>ß<sub><small>w</small></sub> = " +
                                Double.toString(Math.round(Vgr_W * 10000.0) / 10000.0)));
                    } else {
                        Ausgabe.setText(Html.fromHtml("Flächen: " + fn + "<br>s'<sub><small>" +
                                fn + "</small></sub> = " +
                                Double.toString(Math.round(A * 10000.0) / 10000.0) +
                                " mm<br><br>" + "ß = " +
                                Double.toString(Math.round(Vgr * 10000.0) / 10000.0) +
                                "<br>ß<sub><small>t</small></sub> = " +
                                Double.toString(Math.round(Vgr_T * 10000.0) / 10000.0) +
                                "<br>ß<sub><small>w</small></sub> = " +
                                Double.toString(Math.round(Vgr_W * 10000.0) / 10000.0)));
                        Ausgabe_zwischenergebnisse = (TextView) findViewById(zwischenspeicher);
                    }
                }
            }
        });
    }

        }