Changes from version 1.02 to 1.10

- Added BackVertexDistance Conversion (+/- cylinder)
- Bugfixes for various partial programs
- Update of target SDK from 24 to 26 (8.x+)
- Updated privacy policy
- Deleted ExpandableTextView in Info
- Added licence