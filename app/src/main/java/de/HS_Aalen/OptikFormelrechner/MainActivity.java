package de.HS_Aalen.OptikFormelrechner;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        ImageView keyvisual = (ImageView) findViewById(R.id.keyvisual);

        // Zeige das KeyVisual-Image nur im Porträtmodus und nicht im Landscape
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            keyvisual.setVisibility(View.GONE);
        }
        else {
            keyvisual.setVisibility(View.VISIBLE);
        }

        ListView listView = (ListView) findViewById(R.id.ListView1);

        final String liste []= {
                "Paraxiales Ray-Tracing",
                "Exaktes Ray-Tracing für Objekt auf opt. Achse",
                "Schiefgekreuzte Zylinder",
                "Berechnung von Brechungsindices nach der Schottgleichung",
                "Relative Transmission",
                "Linse bester Form / komafreie Linse",
                "Abbildungsfehler eines sph.-tor. Brillenglases",
                "HSA Umrechner",
                "Informationen zum Programm",
        };


        ArrayAdapter <String> adapter = new ArrayAdapter<String>
                (this,android.R.layout.simple_list_item_1, liste);

        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {



            @Override
            public void onItemClick( AdapterView<?> parent, View view, int i, long id) {

                //Weist jedem String Element in "liste" eine Klasse zu, die durch klicken geöffnet
                // wird

                if (i == 0) {
                    Intent s = new Intent(view.getContext(), Raytracing_Start.class);
                    startActivity(s);
                }

                if (i == 1) {
                    Intent s = new Intent(view.getContext(), Exaktes_Raytracing_Start.class);
                    startActivity(s);
                }

                if (i == 2) {
                    Intent s = new Intent(view.getContext(), Powervektoren.class);
                    startActivity(s);
                }

                if (i == 3) {
                    Intent s = new Intent(view.getContext(), Schott.class);
                    startActivity(s);
                }

                if (i == 4) {
                    Intent s = new Intent(view.getContext(), Transmission_Start.class);
                    startActivity(s);
                }

                if (i == 5) {
                    Intent s = new Intent(view.getContext(), Shape.class);
                    startActivity(s);
                }

                if (i == 6) {
                    Intent s = new Intent(view.getContext(), Glasberechnung.class);
                    startActivity(s);
                }

                if (i == 7) {
                    Intent s = new Intent(view.getContext(), HSA_Umrechner.class);
                    startActivity(s);
                }

                if (i == 8) {
                    Intent s = new Intent(view.getContext(), Info.class);
                    startActivity(s);
                }
            }
        });

    }
}
