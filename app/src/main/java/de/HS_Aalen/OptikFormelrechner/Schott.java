package de.HS_Aalen.OptikFormelrechner;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


public class Schott extends AppCompatActivity {

    Spinner Auswahlglas, Farbwahl;
    ArrayAdapter<CharSequence> adapter;


    final Context context = this;
    EditText et_wellenlaenge, et_a0, et_a1, et_a2, et_a3, et_a4, et_a5,  et;
    TextView n, txt_result;
    Button bt;
    double a0, a1, a2, a3, a4, a5, lambda, result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schott);

        // Verhindert das Öffnen des Tastaturlayouts beim Starten der Activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Auswahlglas = (Spinner) findViewById(R.id.Auswahlglas);
        adapter = ArrayAdapter.createFromResource(this, R.array.glasarten,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Auswahlglas.setAdapter(adapter);
        Auswahlglas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                et_a0 = (EditText) findViewById(R.id.et_a0);
                et_a1 = (EditText) findViewById(R.id.et_a1);
                et_a2 = (EditText) findViewById(R.id.et_a2);
                et_a3 = (EditText) findViewById(R.id.et_a3);
                et_a4 = (EditText) findViewById(R.id.et_a4);
                et_a5 = (EditText) findViewById(R.id.et_a5);
                if (position == 0) {
                    et_a0.setText("2.4218304");
                    et_a1.setText("-0.0092167103");
                    et_a2.setText("0.013821685");
                    et_a3.setText("0.00032955714");
                    et_a4.setText("-0.000012153641");
                    et_a5.setText("0.0000010333767");

                    et_a0.setEnabled(false);
                    et_a1.setEnabled(false);
                    et_a2.setEnabled(false);
                    et_a3.setEnabled(false);
                    et_a4.setEnabled(false);
                    et_a5.setEnabled(false);
                } else if (position == 1) {
                    et_a0.setText("2.2718929");
                    et_a1.setText("-0.010108077");
                    et_a2.setText("0.010592509");
                    et_a3.setText("0.00020816965");
                    et_a4.setText("-0.0000076472538");
                    et_a5.setText("0.00000049240991");

                    et_a0.setEnabled(false);
                    et_a1.setEnabled(false);
                    et_a2.setEnabled(false);
                    et_a3.setEnabled(false);
                    et_a4.setEnabled(false);
                    et_a5.setEnabled(false);
                } else if (position == 2) {
                    et_a0.setText("2.2850299");
                    et_a1.setText("-0.0086010725");
                    et_a2.setText("0.011806783");
                    et_a3.setText("0.00020765657");
                    et_a4.setText("-0.0000021314913");
                    et_a5.setText("0.00000032131234");

                    et_a0.setEnabled(false);
                    et_a1.setEnabled(false);
                    et_a2.setEnabled(false);
                    et_a3.setEnabled(false);
                    et_a4.setEnabled(false);
                    et_a5.setEnabled(false);
                } else if (position == 3) {
                    et_a0.setText("2.8784725");
                    et_a1.setText("-0.010565453");
                    et_a2.setText("0.033279420");
                    et_a3.setText("0.0020551378");
                    et_a4.setText("-0.00011396226");
                    et_a5.setText("0.000016340021");

                    et_a0.setEnabled(false);
                    et_a1.setEnabled(false);
                    et_a2.setEnabled(false);
                    et_a3.setEnabled(false);
                    et_a4.setEnabled(false);
                    et_a5.setEnabled(false);
                } else {
                    et_a0.setText("");
                    et_a1.setText("");
                    et_a2.setText("");
                    et_a3.setText("");
                    et_a4.setText("");
                    et_a5.setText("");

                    et_a0.setEnabled(true);
                    et_a1.setEnabled(true);
                    et_a2.setEnabled(true);
                    et_a3.setEnabled(true);
                    et_a4.setEnabled(true);
                    et_a5.setEnabled(true);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Farbwahl = (Spinner) findViewById(R.id.Farbauswahl);
        adapter = ArrayAdapter.createFromResource(this, R.array.farbwahl,
                android.R.layout.simple_list_item_1);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Farbwahl.setAdapter(adapter);
        Farbwahl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 18) {
                    et_wellenlaenge = (EditText) findViewById(R.id.et_wellenlaenge);
                    et_wellenlaenge.setEnabled(true);
                    et_wellenlaenge.setText("");
                } else {
                    et_wellenlaenge = (EditText) findViewById(R.id.et_wellenlaenge);

                    if (position == 0) {
                        et_wellenlaenge.setText("365.0");
                    } else if (position == 1) {
                        et_wellenlaenge.setText("404.7");
                    } else if (position == 2) {
                        et_wellenlaenge.setText("435.8");
                    } else if (position == 3) {
                        et_wellenlaenge.setText("480.0");
                    } else if (position == 4) {
                        et_wellenlaenge.setText("486.1");
                    } else if (position == 5) {
                        et_wellenlaenge.setText("546.1");
                    } else if (position == 6) {
                        et_wellenlaenge.setText("587.6");
                    } else if (position == 7) {
                        et_wellenlaenge.setText("589.3");
                    } else if (position == 8) {
                        et_wellenlaenge.setText("632.8");
                    } else if (position == 9) {
                        et_wellenlaenge.setText("643.8");
                    } else if (position == 10) {
                        et_wellenlaenge.setText("656.3");
                    } else if (position == 11) {
                        et_wellenlaenge.setText("706.5");
                    } else if (position == 12) {
                        et_wellenlaenge.setText("852.1");
                    } else if (position == 13) {
                        et_wellenlaenge.setText("1014.0");
                    } else if (position == 14) {
                        et_wellenlaenge.setText("1060.0");
                    } else if (position == 15) {
                        et_wellenlaenge.setText("1529.6");
                    } else if (position == 16) {
                        et_wellenlaenge.setText("1970.1");
                    } else if (position == 17) {
                        et_wellenlaenge.setText("2325.4");
                    }
                    et_wellenlaenge.setEnabled(false);
                    n = (TextView) findViewById(R.id.txt_n);
                    n.setText(Html.fromHtml("n<sub><small>" + et_wellenlaenge.getText().toString() +
                            "</small></sub>:"));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bt = (Button) findViewById(R.id.bT);
        bt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String Alarm = et_a0.getText().toString();
                if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                        Alarm.equals("+")) {
                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                    AlarmBox.setTitle("Leeres Feld!");
                    AlarmBox.setMessage(Html.fromHtml("A<sub><small>0</small>" +
                            "</sub> ist leer<br>"));
                    AlarmBox.setNeutralButton("OK", null);

                    AlertDialog dialog = AlarmBox.create();
                    dialog.show();
                    return;
                }
                Alarm = et_a1.getText().toString();
                if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                        Alarm.equals("+")) {
                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                    AlarmBox.setTitle("Leeres Feld!");
                    AlarmBox.setMessage(Html.fromHtml("A<sub><small>1</small>" +
                            "</sub> ist leer<br>"));
                    AlarmBox.setNeutralButton("OK", null);

                    AlertDialog dialog = AlarmBox.create();
                    dialog.show();
                    return;
                }
                Alarm = et_a2.getText().toString();
                if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                        Alarm.equals("+")) {
                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                    AlarmBox.setTitle("Leeres Feld!");
                    AlarmBox.setMessage(Html.fromHtml("A<sub><small>2</small>" +
                            "</sub> ist leer<br>"));
                    AlarmBox.setNeutralButton("OK", null);

                    AlertDialog dialog = AlarmBox.create();
                    dialog.show();
                    return;
                }
                Alarm = et_a3.getText().toString();
                if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                        Alarm.equals("+")) {
                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                    AlarmBox.setTitle("Leeres Feld!");
                    AlarmBox.setMessage(Html.fromHtml("A<sub><small>3</small>" +
                            "</sub> ist leer<br>"));
                    AlarmBox.setNeutralButton("OK", null);

                    AlertDialog dialog = AlarmBox.create();
                    dialog.show();
                    return;
                }
                Alarm = et_a4.getText().toString();
                if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                        Alarm.equals("+")) {
                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                    AlarmBox.setTitle("Leeres Feld!");
                    AlarmBox.setMessage(Html.fromHtml("A<sub><small>4</small>" +
                            "</sub> ist leer<br>"));
                    AlarmBox.setNeutralButton("OK", null);

                    AlertDialog dialog = AlarmBox.create();
                    dialog.show();
                    return;
                }
                Alarm = et_a5.getText().toString();
                if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                        Alarm.equals("+")) {
                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                    AlarmBox.setTitle("Leeres Feld!");
                    AlarmBox.setMessage(Html.fromHtml("A<sub><small>5</small>" +
                            "</sub> ist leer<br>"));
                    AlarmBox.setNeutralButton("OK", null);

                    AlertDialog dialog = AlarmBox.create();
                    dialog.show();
                    return;
                }

                Alarm = et_wellenlaenge.getText().toString();
                if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                        Alarm.equals("+")) {
                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                    AlarmBox.setTitle("Leeres Feld!");
                    AlarmBox.setMessage(Html.fromHtml("Das Feld für die Wellenlänge ist leer"));
                    AlarmBox.setNeutralButton("OK", null);

                    AlertDialog dialog = AlarmBox.create();
                    dialog.show();
                    return;
                }

                lambda = Double.parseDouble(et_wellenlaenge.getText().toString());
                n.setText(Html.fromHtml("n<sub><small>" +
                        Double.toString(Math.round(lambda*10)/10.0) + "</small></sub>:"));
                lambda = lambda/1000;
                a0 = Double.parseDouble(et_a0.getText().toString());
                a1 = Double.parseDouble(et_a1.getText().toString());
                a2 = Double.parseDouble(et_a2.getText().toString());
                a3 = Double.parseDouble(et_a3.getText().toString());
                a4 = Double.parseDouble(et_a4.getText().toString());
                a5 = Double.parseDouble(et_a5.getText().toString());

                result = Math.sqrt(a0 + a1 * Math.pow(lambda, 2) + (a2 / Math.pow(lambda, 2)) +
                        (a3 / Math.pow(lambda, 4)) + (a4 / Math.pow(lambda, 6)) +
                        (a5 / Math.pow(lambda, 8)));

                result = Math.round(result * 100000) / 100000.0;

                txt_result = (TextView) findViewById(R.id.txt_result);
                txt_result.setText(Double.toString(result));

                }
        });
    }
}