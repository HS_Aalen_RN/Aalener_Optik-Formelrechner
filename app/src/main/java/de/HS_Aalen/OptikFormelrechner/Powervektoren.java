package de.HS_Aalen.OptikFormelrechner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;

import static de.HS_Aalen.OptikFormelrechner.R.id.Glas_1;
import static de.HS_Aalen.OptikFormelrechner.R.id.Glas_2;


public class Powervektoren extends AppCompatActivity {

    EditText sph1, sph2, cyl1, cyl2, achse1, achse2;
    TextView sph3, cyl3, achse3, Glas1, Glas2;
    int Glaszaehler = 3;

    // Erstellt einen Filter der die Eingabe der Achsen auf 0-179° beschränkt
    public class InputFilterMinMax implements InputFilter {
        private int min;
        private int max;

        public InputFilterMinMax(int min, int max) {
            this.min = min;
            this.max = max;
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest,
                                   int dstart, int dend) {

            try {
                double input = Double.parseDouble(dest.subSequence(0, dstart).toString() +
                        source + dest.subSequence(dend, dest.length()));
                if (isInRange(min, max, input))
                    return null;
            } catch (NumberFormatException nfe) { }
            return "";
        }

        private boolean isInRange(double a, double b, double c) {
            return b > a ? c >= a && c <= b : c >= b && c <= a;
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.powervektoren);

        // Verhindert das Öffnen des Tastaturlayouts beim Starten der Activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        sph1 = (EditText) findViewById(R.id.in_sph_1);
        sph2 = (EditText) findViewById(R.id.in_sph_2);
        cyl1 = (EditText) findViewById(R.id.in_cyl_1);
        cyl2 = (EditText) findViewById(R.id.in_cyl_2);
        achse1 = (EditText) findViewById(R.id.in_achse_1);
        achse1.setFilters(new InputFilter[]{new InputFilterMinMax(0, 179)});
        achse2 = (EditText) findViewById(R.id.in_achse_2);
        achse2.setFilters(new InputFilter[]{new InputFilterMinMax(0, 179)});

        sph3 = (TextView) findViewById(R.id.out_sph_3);
        cyl3 = (TextView) findViewById(R.id.out_cyl_3);
        achse3 = (TextView) findViewById(R.id.out_achse_3);

        Glas1 = (TextView) findViewById(Glas_1);
        Glas2 = (TextView) findViewById(Glas_2);


        Button Berechnen = (Button)findViewById(R.id.button);
        final Button Clear = (Button)findViewById(R.id.bt_clear);
        final Button plus_glas = (Button)findViewById(R.id.bt_plus);

        Berechnen.setOnClickListener(
                new Button.OnClickListener(){
                    public void onClick(View v) {
                        String Alarm = sph2.getText().toString();
                        String Alarm2 = cyl2.getText().toString();
                        if (Alarm.equals("") && Alarm2.equals("")) {
                            return;
                        } else {
                            Alarm = sph1.getText().toString();
                            if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                sph1.setText("0");
                            }
                            double b_sph1 = Double.parseDouble(sph1.getText().toString());
                            Alarm = sph2.getText().toString();
                            if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                sph2.setText("0");
                            }
                            double b_sph2 = Double.parseDouble(sph2.getText().toString());
                            Alarm = cyl1.getText().toString();
                            if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                cyl1.setText("0");
                            }
                            double b_cyl1 = Double.parseDouble(cyl1.getText().toString());
                            Alarm = cyl2.getText().toString();
                            if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                cyl2.setText("0");
                            }
                            double b_cyl2 = Double.parseDouble(cyl2.getText().toString());
                            Alarm = achse1.getText().toString();
                            if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                achse1.setText("0");
                            }
                            double b_achse1 = Double.parseDouble(achse1.getText().toString());
                            Alarm = achse2.getText().toString();
                            if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                    Alarm.equals("+")) {
                                achse2.setText("0");
                            }
                            double b_achse2 = Double.parseDouble(achse2.getText().toString());

                            if (b_cyl1 < 0) {
                                b_sph1 = b_sph1 + b_cyl1;
                                b_cyl1 = b_cyl1 * (-1);
                                b_achse1 = b_achse1 + 90;
                                if (b_achse1 > 179) {
                                    b_achse1 = b_achse1 - 180;
                                }
                            }

                            if (b_cyl2 < 0) {
                                b_sph2 = b_sph2 + b_cyl2;
                                b_cyl2 = b_cyl2 * (-1);
                                b_achse2 = b_achse2 + 90;
                                if (b_achse2 > 179) {
                                    b_achse2 = b_achse2 - 180;
                                }
                            }

                            double M1 = b_sph1 + (b_cyl1 / 2);
                            double M2 = b_sph2 + (b_cyl2 / 2);

                            double M = M1 + M2;

                            double rad_achse1 = Math.toRadians(b_achse1 * 2);
                            double rad_achse2 = Math.toRadians(b_achse2 * 2);

                            double I0_1 = -(b_cyl1 / 2) * Math.cos(rad_achse1);
                            double I0_2 = -(b_cyl2 / 2) * Math.cos(rad_achse2);

                            double I0 = I0_1 + I0_2;

                            double I45_1 = -(b_cyl1 / 2) * Math.sin(rad_achse1);
                            double I45_2 = -(b_cyl2 / 2) * Math.sin(rad_achse2);

                            double I45 = I45_1 + I45_2;

                            double b_cyl3 = 2 * Math.sqrt(Math.pow(I0, 2) + Math.pow(I45, 2));
                            b_cyl3 = Math.round(b_cyl3 * 10000) / 10000.0;
                            double b_sph3 = M - (b_cyl3 / 2);
                            b_sph3 = Math.round(b_sph3 * 10000) / 10000.0;
                            double b_achse3 = (Math.atan(I45 / I0)) / 2;

                            if (b_cyl3 == 0) {
                                achse3.setText("0");
                            } else {
                                b_achse3 = Math.round(Math.toDegrees(b_achse3) * 10000) / 10000.0;
                            }

                            if (I0 > 0) {
                                b_achse3 = b_achse3 + 90;
                            }

                            if (b_achse3 < 0) {
                                b_achse3 = b_achse3 + 180;
                            }

                            sph3.setText(Double.toString(b_sph3));
                            String Ausgabe = sph3.getText().toString();
                            if (Ausgabe.length() > 9) {
                                sph3.setText(String.format("%9.4e", b_sph3).replace(",","."));
                            }

                            cyl3.setText(Double.toString(b_cyl3));
                            Ausgabe = cyl3.getText().toString();
                            if (Ausgabe.length() > 9) {
                                cyl3.setText(String.format("%9.4e" , b_cyl3).replace(",","."));
                            }
                            if (b_cyl3 != 0) {
                                achse3.setText(Double.toString(b_achse3));
                            }

                        }
                    }
                }
        );

        Clear.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        sph1.setEnabled(true);
                        sph1.setText("");
                        sph2.setText("");
                        sph3.setText("");

                        cyl1.setEnabled(true);
                        cyl1.setText("");
                        cyl2.setText("");
                        cyl3.setText("");

                        achse1.setEnabled(true);
                        achse1.setText("");
                        achse1.setFilters(new InputFilter[]{new InputFilterMinMax(0, 179)});
                        achse2.setText("");
                        achse2.setFilters(new InputFilter[]{new InputFilterMinMax(0, 179)});
                        achse3.setText("");

                        plus_glas.setText("+ 3. Glas");
                        Glaszaehler = 3;
                        Glas1.setText("Glas 1");
                        Glas2.setText("Glas 2");
                    }
                }
        );

        plus_glas.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        String Alarm = sph3.getText().toString();
                        if (Alarm.isEmpty()) {
                            return;
                        } else {
                            sph1.setEnabled(true);
                            sph1.setText(sph3.getText());
                            sph1.setEnabled(false);
                            cyl1.setEnabled(true);
                            cyl1.setText(cyl3.getText());
                            cyl1.setEnabled(false);
                            achse1.setEnabled(true);
                            achse1.setFilters(new InputFilter[] {});
                            achse1.setText(achse3.getText());
                            achse1.setFilters(new InputFilter[]{new InputFilterMinMax(0, 179)});
                            achse1.setEnabled(false);

                            sph2.setText("");
                            sph3.setText("");
                            cyl2.setText("");
                            cyl3.setText("");
                            achse2.setText("");
                            achse2.setFilters(new InputFilter[]{new InputFilterMinMax(0, 179)});
                            achse3.setText("");

                            Glas1.setText("Berechnetes Glas");
                            Glas2.setText("Glas " + Glaszaehler);
                            Glaszaehler = Glaszaehler + 1;
                            plus_glas.setText("+ " + Glaszaehler + ". Glas");

                        }

                    }
                }
        );
    }
}
