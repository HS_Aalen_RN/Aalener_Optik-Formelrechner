Änderungen von Version 1.11 auf 1.12

- Update zu neuer Ziel-API von 31 zu 35 (Android 12 zu 15)
- Update zu neuer minimaler API von 15 zu 21 (Android 4.0.3 zu 5.0)
- Änderungen bei der Textgröße und beim unteren Zeilenabstand bestimmter Textfelder, die zuvor abgeschnitten dargestellt wurden, um die Lesbarkeit zu verbessern.
- Überarbeitung von Legacy-Funktionen die nicht länger unterstützt werden
- Lizenz aktualisiert